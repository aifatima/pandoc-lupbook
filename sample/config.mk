
# bootstrap
MODULE_DEPS += ext/bootstrap/dist/js/bootstrap.min.js
MODULE_DEPS += ext/bootstrap/dist/css/bootstrap.min.css

# codemirror
MODULE_DEPS += ext/codemirror/lib/codemirror.js
MODULE_DEPS += ext/codemirror/lib/codemirror.css
MODULE_DEPS += ext/codemirror/addon/edit/matchbrackets.js
MODULE_DEPS += ext/codemirror/mode/clike/clike.js

# xterm
MODULE_DEPS += ext/xtermjs/lib/xterm.js
MODULE_DEPS += ext/xtermjs/css/xterm.css
